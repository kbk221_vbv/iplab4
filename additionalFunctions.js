/**
 * Tasks additional functions
 */

const getDayRealNumber = (date) => {
    const day = date.getDay();
    return day === 0 ? 7 : day;
}
    

const GetDayOfWeek = (date) => {
    const days = ['неділя', 'понеділок', 'вівторок', 'середа', 'четвер', 'п\'ятниця', 'субота'];
    return days[date.getDay()];
}

const GetDayOfMonthAndYear = (date) => {
    const day = date.getDate();
    const month = date.getMonth();
    const monthName = GetMonthName(month);
    const year = date.getFullYear();
    return `${day} ${monthName} ${year} року`;
}

const GetMonthName = (month) => {
    const months = ['січеня', 'лютого', 'березня', 'квітня', 'травня', 'червня', 'липня', 'серпня', 'вересня', 'жовтня', 'листопада', 'грудня'];
    return months[month];
}

function DateOnSimpleFormat(date) {
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const seconds = date.getSeconds();
    return `${day}.${month}.${year} ${hours}:${minutes}:${seconds}`;
}

function ParseDateRegularExpression(date) {
    const reg = /(\d{2})((\.)|(-)|(\/))(\d{2})((\.)|(-)|(\/))(\d{4}) (\d{2}):(\d{2}):(\d{2})/;
    let result = reg.exec(date);
    try {
        return DateOnSimpleFormat(new Date(result[11], result[6] - 1, result[1], result[12], result[13], result[14]));
    } catch (error) {
        return 'Невірний формат дати';
    }
}

export { getDayRealNumber, GetDayOfWeek, GetDayOfMonthAndYear, GetMonthName, DateOnSimpleFormat, ParseDateRegularExpression };