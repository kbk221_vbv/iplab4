import { getDayRealNumber, GetDayOfWeek, GetDayOfMonthAndYear, DateOnSimpleFormat, ParseDateRegularExpression } from './additionalFunctions.js';

const firstTaskButton = document.getElementById('firstTaskButton');
const secondTaskButton = document.getElementById('secondTaskButton');
const thirdTaskButton = document.getElementById('thirdTaskButton');
const fourthTaskButton = document.getElementById('fourthTaskButton');
const fifthTaskButton = document.getElementById('fifthTaskButton');
const sixthTaskButton = document.getElementById('sixthTaskButton');
const seventhTaskButton = document.getElementById('seventhTaskButton');
const eighthTaskButton = document.getElementById('eighthTaskButton');
const ninthTaskButton = document.getElementById('ninthTaskButton');
const tenTaskButton = document.getElementById('tenthTaskButton');

/**
 * Tasks results elements
 */

const firstTaskResult = document.getElementById('first-task-result');
const secondTaskResult = document.getElementById('second-task-result');
const thirdTaskResult = document.getElementById('third-task-result');
const fourthTaskResult = document.getElementById('fourth-task-result');
const fifthTaskResult = document.getElementById('fifth-task-result');
const sixthTaskResult = document.getElementById('sixth-task-result');
const seventhTaskResult = document.getElementById('seventh-task-result');
const eighthTaskResult = document.getElementById('eighth-task-result');
const ninthTaskResult = document.getElementById('ninth-task-result');
const tenTaskResult = document.getElementById('ten-task-result');

/**
 * Tasks main handlers
 */

firstTaskButton.onclick = GetCurrentDateInfoAndPasteToResult;
secondTaskButton.onclick = GetDayNumberAndDayNameOfWeek;
thirdTaskButton.onclick = FindDateNDaysAgo;
fourthTaskButton.onclick = LastDayOfMonthByYearAndMonth;
fifthTaskButton.onclick = CountOfSecondsFromStartOfTheDayAndToEndOfTheDay;
sixthTaskButton.onclick = PrintDateOnSimpleFormat;
seventhTaskButton.onclick = PrintDifferenceBetweenDates;
eighthTaskButton.onclick = ReactionOnDateInput;
ninthTaskButton.onclick = InputStringToDate;
tenTaskButton.onclick = TranslateToLocaleDateString;

/**
 * Tasks main functions
 */

function GetCurrentDateInfoAndPasteToResult() {
    const date = new Date();
    const day = GetDayOfWeek(date);
    const time = date.toLocaleTimeString();
    const stringDate = GetDayOfMonthAndYear(date);
    firstTaskResult.innerText = `Дата: ${stringDate} \nДень: ${day} \nЧас: ${time}`;
}

function GetDayNumberAndDayNameOfWeek() {
    const date = new Date();
    secondTaskResult.innerText = `Номер дня: ${getDayRealNumber(date)} \nНазва дня: ${GetDayOfWeek(date)}`;
}

function FindDateNDaysAgo() {
    const valueThirdtaskInput = document.getElementById('third-task-number').value;
    const daysAgo = valueThirdtaskInput;
    const date = new Date();
    const days = date.getDate();
    date.setDate(days - daysAgo);
    thirdTaskResult.innerText = `Дата: ${GetDayOfMonthAndYear(date)} \nДень: ${GetDayOfWeek(date)} \nЧас: ${date.toLocaleTimeString()}`;
    console.log(valueThirdtaskInput);
}

function LastDayOfMonthByYearAndMonth() {
    const year = document.getElementById('fourth-year-number').value;
    const month = document.getElementById('fourth-month-number').value;
    const date = new Date(year, month, 0);
    fourthTaskResult.innerText = `Останній день місяця: ${GetDayOfMonthAndYear(date)} \nДень: ${GetDayOfWeek(date)}`;
}

function CountOfSecondsFromStartOfTheDayAndToEndOfTheDay() {
    const date = new Date();
    const seconds = date.getSeconds();
    const minutes = date.getMinutes();
    const hours = date.getHours();
    const secondsFromStart = hours * 3600 + minutes * 60 + seconds;
    const secondsToEnd = 86400 - secondsFromStart;
    fifthTaskResult.innerText = `Секунд з початку доби: ${secondsFromStart} \nСекунд до кінця доби: ${secondsToEnd}`;
}

function PrintDateOnSimpleFormat() {
    const date = new Date(document.getElementById('sixth-task-date').value);
    sixthTaskResult.innerText = DateOnSimpleFormat(date);
}

function PrintDifferenceBetweenDates() {
    const date1 = new Date(document.getElementById('seventh-task-first-date').value);
    const date2 = new Date(document.getElementById('seventh-task-second-date').value);
    const difference = Math.abs(date1 - date2);
    const days = Math.floor(difference / 1000 / 60 / 60 / 24);
    const hours = Math.floor(difference / 1000 / 60 / 60) % 24;
    const minutes = Math.floor(difference / 1000 / 60) % 60;
    const seconds = Math.floor(difference / 1000) % 60;
    seventhTaskResult.innerText = `Різниця між датами: ${days} днів, ${hours} годин, ${minutes} хвилин, ${seconds} секунд`;
}

function ReactionOnDateInput() {
    const date1 = new Date(document.getElementById('eighth-task-date').value);
    const date2 = new Date();
    const difference = Math.abs(date1 - date2);
    let result = '';
    if (difference < 1000) {
        result = 'Тільки що';
    }
    else if (difference < 60000) {
        result = `${Math.floor(difference / 1000)} секунд тому`;
    }
    else if (difference < 3600000) {
        result = `${Math.floor(difference / 1000 / 60)} хвилин тому`;
    }
    else {
        result = `${DateOnSimpleFormat(date1)}\n${DateOnSimpleFormat(date2)}`;
    }
    eighthTaskResult.innerText = result;
}

function InputStringToDate() {
    let result = '';
    const date = new Date(document.getElementById('ninth-task-date').value);
    if(date.toString() === 'Invalid Date') {
        result = ParseDateRegularExpression(document.getElementById('ninth-task-date').value);
    }
        
    ninthTaskResult.innerText = result;
}

function TranslateToLocaleDateString() {
    const date = new Date();
    const language = document.getElementById('ten-task-language').value;
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    try {
        tenTaskResult.innerText = date.toLocaleDateString(language, options);
    }
    catch (e) {
        tenTaskResult.innerText = 'Введено невірний код мови';
    }
}

